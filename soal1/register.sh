#!/bin/bash
# shellcheck disable=SC2164
cd "$(pwd)"
echo "register"
while true
do
	printf "masukan username: "; read u
if [ -e "./users/user.txt" ] && grep -q "$u:" ./users/user.txt
then
	echo "username already exists!"
	echo ""
	echo "$(date '+%m/%d/%Y %H/%M/%S') REGISTER: ERROR User already exists" >> log.txt
continue
fi
	printf "masukan password: "; read -s s
	echo ""
if ! [[ ${#s} -ge 8 && "$s" == *[A-Z]* && "$s" == *[a-z]* && "$s" == *[0-9]* && "$u" != "$s" ]]
then
	echo "password harus memenuhi kriteria sebagai berikut:"
	echo "1. Minimal 8 karakter"
	echo "2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil"
	echo "3. Alphanumeric"
	echo "4. Tidak boleh sama dengan username"
	echo ""
else
break
fi
done
mkdir -p users
echo "$u:$s" >> "./users/user.txt"
echo "$(date '+%m/%d/%Y %H:%M:%S') REGISTER: INFO User $u registered successfuly" >> log.txt
