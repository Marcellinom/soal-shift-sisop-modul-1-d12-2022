#!/bin/bash
# shellcheck disable=SC2164
cd "$(pwd)"
function err() {
	echo "login gagal"
	echo "$(date '+%m/%d/%Y %H:%M:%S') LOGIN: WARNING Failed login attempt on user $1" >> log.txt
}
echo "login"
while true
do
	printf "masukan username: "; read u
	printf "masukan password: "; read -s s
	echo ""
	if [ -e "./users/user.txt" ]
	then
		if ! grep -q "$u:"  ./users/user.txt
		then
			err $u
			continue
		fi
		IFS=':'
		d=$( grep "$u:" ./users/user.txt )
		read -a data <<< "$d"
		if [ "${data[1]}" == "$s" ]
		then
			echo "login berhasil"
			echo "$(date '+%m/%d/%Y %H:%M:%S') LOGIN: INFO User $u logged in" >> log.txt
			break
		else
			err $u
		fi
	fi
done

function masukin() {
	folder=$3
	for ((i=$1; i<$1+$2; i=i+1))
	do
		num=$(printf "%02d" "$i")
		curl --progress-bar -L  https://loremflickr.com/320/240 -o "$folder/PIC_$num.jpg"
		tput cuu1
		tput el
	done
}

function donlot() {
	folder="$(date '+%Y-%m-%d')_$u"
	if [ -e "$folder.zip" ]
	then
		unzip -P "$s" "$folder.zip"
		numfiles=(*)
		numfiles=${#numfiles[@]}
		masukin $numfiles-1 $1 $folder
	else
		mkdir "$folder"
		masukin 1 "$1" "$folder"
	fi
	zip -P "$s" -r "$folder.zip" "$folder"
	rm -fr "$folder"
}

IFS=' ' read -ra args
case "${args[0]}" in
dl)
donlot "${args[1]}"
;;
att)
awk -v name=$u '$0 ~ name && /LOGIN/ { ++n } END { print n }' log.txt
;;
esac
