#!/bin/bash
# shellcheck disable=SC2164
cd "$(pwd)"

mkdir -p forensic_log_website_daffainfo_log
#nge split inputnya
splitted=$(awk -F'["]' '{ for(i=2;i<=NF;i++) { if($i == ":") continue; printf "%s ",  $i} printf "\n"}' log_website_daffainfo.log | awk '(NR>1)')
#rata rata serangan per jam
echo "$splitted" | awk -F'[:]' '{absen[$2]++} END {for (hadir in absen) { jam++; total+=0+absen[hadir] } printf "Rata-rata serangan adalah sebanyak %.3f requests per jam\n", 0+total/(jam-1)}' > forensic_log_website_daffainfo_log/ratarata.txt
#ip paling banyak akses ke server
echo "$splitted" | awk '{ips[$1]++} END {maks_req=0; for (ip in ips) { if (maks_req < ips[ip]) {maks_req = ips[ip]; maks_ip = ip} } printf "IP yang paling banyak mengakses server adalah: %s sebanyak %s requests\n\n", maks_ip, maks_req }' > forensic_log_website_daffainfo_log/result.txt
#yg pake user agent curl
echo "$splitted" | awk '$NF~ /curl/ {sum++} END {printf "Ada %s requests yang menggunakan curl sebagai user-agent\n\n", sum}' >> forensic_log_website_daffainfo_log/result.txt
#yg request jam 2 pagi tanggal 22
echo "$splitted" | awk -F'[/ ]' '$4~ /2022:02/ {printf "%s Jam 2 pagi\n", $1}' >> forensic_log_website_daffainfo_log/result.txt