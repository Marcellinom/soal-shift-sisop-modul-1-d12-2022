# Soal-Shift-Sisop-Modul-1-D12-2022
## Anggota Kelompok
* Marcellino Mahesa Janitra 5025201105
* Afiq Akram 5025201270
* Samuel Berkat Hulu 5025201055
## Kendala
pas setting cron:

![crying](https://cdn.discordapp.com/attachments/810347347237273631/947131042949644318/crying-crying-man.gif)

### Soal 1
- agak tricky buat processing zip unzip nya

### Soal 2
- gaada

### Soal 3
- susah configurasi cron di bash 
- akhirnya harus di configure lewat terminal
- itu pun engga jalan di local 

## Cara run
- **Soal 1**
1. Register
```shell
$ bash register.sh
```
![img.png](https://media.discordapp.net/attachments/810347347237273631/947107641841307708/unknown.png?width=515&height=53)

2. Login
```shell
$ bash main.sh
```
![img2.png](https://cdn.discordapp.com/attachments/810347347237273631/947108368542224454/unknown.png)

3. dl \<jumlah file yg mau di donlot:int>
![img3.png](https://cdn.discordapp.com/attachments/810347347237273631/947108730804244492/unknown.png)

4. att

![img.png](https://cdn.discordapp.com/attachments/810347347237273631/947109253150285824/unknown.png)

- **Soal 2**
1. Execute
```shell
$ bash soal2_forensic_dapos.sh
```
**Output:**

![img.png](https://cdn.discordapp.com/attachments/810347347237273631/947110134537134090/unknown.png)
![img_1.png](https://cdn.discordapp.com/attachments/810347347237273631/947110522187288596/unknown.png)

- **Soal 3**
1. Configure cron buat run script nya
```shell
$ (crontab -l 2>/dev/null ; path=$(pwd) ; echo "*/1 * * * * $path/minute_log.sh") | crontab -
$ (crontab -l 2>/dev/null ; path=$(pwd) ; echo "@hourly $path/aggregate_minutes_to_hourly_log.sh") | crontab -
```
2. Restart cron
```shell
$ sudo service cron restart
```
3. Output minute_log per menit di folder
![img_2.png](https://cdn.discordapp.com/attachments/810347347237273631/947117164224213002/unknown.png)
4. Output file
![img_4.png](https://cdn.discordapp.com/attachments/810347347237273631/947127206893735996/unknown.png)
5. Output aggregate_hourly per jam di folder
![img_3.png](https://cdn.discordapp.com/attachments/810347347237273631/947126924172480613/unknown.png)
6. Output file
![img_5.png](https://cdn.discordapp.com/attachments/810347347237273631/947127603683287100/unknown.png)
7. 
## Solusi

### Soal 1

Soal ini terbagi menjadi dua file yaitu ```register.sh``` dan ```main.sh```. ```register.sh``` berfungsi untuk membuat dan menyimpan data user baru, sedangkan ```main.sh``` untuk melakukan perintah command yang diminta.

Pertama kita menggunakan while loop, print username lalu diread dan dinisialisasi sebagai ```u```. Kemudian dicek apakah username tersedia atau tidak. 
```shell
if [ -e "./users/user.txt" ] && grep -q "$u:" ./users/user.txt
```

Apabila tidak tersedia, outpunya ```username already exists!``` lalu akan disimpan di ```log.txt```. Saat berhasil maka username dan password akan disimpan di ```./users/user.txt```. Kemudian dicek terlebih dahulu apakah password sesuai dengan kriteria yang diinginkan.
```

if ! [[ ${#s} -ge 8 && "$s" == *[A-Z]* && "$s" == *[a-z]* && "$s" == *[0-9]* && "$u" != "$s" ]]
```
Apabila tidak sesuai dengan kriteria yang ada, maka outputnya sebagai berikut
```shell
echo "password harus memenuhi kriteria sebagai berikut:"
	echo "1. Minimal 8 karakter"
	echo "2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil"
	echo "3. Alphanumeric"
	echo "4. Tidak boleh sama dengan username"
	echo ""
```
Saat proses register selesai maka tercatat sebagai berikut
```echo "$(date '+%m/%d/%Y %H:%M:%S') REGISTER: INFO User $u registered successfuly" >> log.txt```


Untuk ```main.sh```, kita diminta untuk login kembali dengan username dan password yang sudah diisi di ```register.sh```. Di sini username dan password akan dicocokkan dengan data yang telah tersimpan di ```./users/user.txt``` Dengan kode sebagai berikut
```shell
if [ "${data[1]}" == "$s" ]
		then
			echo "login berhasil"
			echo "$(date '+%m/%d/%Y %H:%M:%S') LOGIN: INFO User $u logged in" >> log.txt
			break
```
Kemudian, dimasukan string nama folder ke variable folder, di sini menggunakan for loop untuk mendownload image berdasarkan jumlah yang diinginkan user.

```shell
for ((i=$1; i<$1+$2; i=i+1))
	do
		num=$(printf "%02d" "$i")
		curl --progress-bar -L  https://loremflickr.com/320/240 -o "$folder/PIC_$num.jpg"
		tput cuu1
		tput el
	done
```
Folder yang berhasil disimpan maka namanya sebagai berikut ```folder="$(date '+%Y-%m-%d')_$u"```. Kemudian, di line 48 dicek apakah ada folder.zip, apabila ada maka diunzip. Setelah itu, variable numfiles berfungsi untuk mengetahui berapa jumlah foto yang ada. Kemudian dimasukan ke ```function masukin```. Terakhir folder tersebut dizip kembali, untuk att disini berfungsi untuk menghitung jumlah percobaan login berhasil maupun tidak dari user yang login sekarang proses ini menggunakan awk.

### Soal 2

Sebuah website dengann alamat https://daffa.info di retas oleh sekelompok orang yang tidak di ketahui. Akibat kejadian tersebut seorang dapot yang bertugas sebagai opertor dari website tersebut harus merelakan hari liburnya untuk mengecek dan memperbaiki kesalahan yang terjadi akibat dari penyerangan mendadak it. karena kejadin tersebut dapos meminta bantun agar kita dapat membaca log website dan memperbaiki permasalahan yang ada.

untuk mengetahui jumlah pennyerangan yang dilakukan, dapos meminta kita untuk membuat sebuah file scrip AWK yang bernama
`` soal2_forensic_dapos.sh `` file ini menyimpan semua data komputer penyerang.
Untuk langka selanjutnya dapos meminta membuatkan sebuah folder bernama `  forensic_log_website_daffainfo_log ` sebagai tempat dari perhitungan rata-rata penyerangan yang dimuat dalam file `` ratarata.txt ``.
dan penyerangan yang dilakukan dapa jam 2 pagi pada tanggal 23 yang di masukan kedalam file `` result.txt ``.
- di soal ini kita diminta untuk mengolah data log dari website dapos
- kita diminta untuk menemukan rata-rata serangan perjam, ip paling banyak yang melakukan request, request yang pakai user agent curl, dan yang melakukan request jam 2 pagi tanggal 22

buat file baru yang isinya semua data komputer penyerang
```shell
	mkdir soal2_forensic_dapos.sh
```

hitung rata-rata penyerangan perjam.
- caranya simpel saja, setelah di split menggunakan awk, kita dapat mendetermine pada jam berapa saja request-request tersebut
- map stiap kehadiran perjam, lalu pada akhirnya jumlah kehadiran dibagi jumlah jam untuk menghasilkan rata2 serangan per jam
```shell
awk -F'[:]' '{absen[$2]++} END {for (hadir in absen) { jam++; total+=0+absen[hadir] } printf "Rata-rata serangan adalah sebanyak %.3f requests per jam\n", 0+total/(jam-1)}'
```
selanjutnya untuk mencari ip address yang paling banyak melakukan serangan ke server dan mecetak banyaknya request yang di kirim.
- untuk ini juga sangat simpel, hanya perlu meng-map jumlah keharidan ip kedalam array
- lalu loop semua arraynya yang berisi jumlah kehadiran seluruh ip, cari yang paling banyak jumlah kehadirannya menggunakan if else sederhana
```shell
awk '{ips[$1]++} END {maks_req=0; for (ip in ips) { if (maks_req < ips[ip]) {maks_req = ips[ip]; maks_ip = ip} } printf "IP yang paling banyak mengakses server adalah: %s sebanyak %s requests\n\n", maks_ip, maks_req }'
```
hitung banyaknya requests yang menggunakan user-agent curl.
- jika dilihat, kolom user agent merupakan kolom terakhir
- kita bisa menggunakan teknik pencarian kata per kolom menggunakan ```$NF~ /curl/```, itu berarti cari di kolom terakhir yang word contains "curl"
- sisanya tinggal dijumlah saja ada berapa occurences
```shell
awk '$NF~ /curl/ {sum++} END {printf "Ada %s requests yang menggunakan curl sebagai user-agent\n\n", sum}'
```
cari tahu banyaknya ip address yang menyerang saat jam 2 pagi \
- split baris menggunakan delimiter "/"
- cari pada kolom keempat, yaitu kolom date time jam 2 pagi 2022
- print semua occurences
```shell
awk -F'[/ ]' '$4~ /2022:02/ {printf "%s Jam 2 pagi\n", $1}'
```
setelah menemukan ip addrees yang paling banyak melakukan penyerangan ke server, requests yang menggunakan user-agent curl, dan ip address yang menyerang jam 2 pagi, semua hasil terebut di isikan kedalam file result.txt. file result.txt dan file ratarata.txt yang menyimpan semua hasil perhitunga di masukan kedalam folder forensic_log_website_daffainfo_log.



### Soal 3 
- pada soal ini diminta untuk menuliskan hasil ```free -m &&  du -sh``` dari directory "/home/user/" ke folder log per menit
- lalu setiap jam akan merekap kumpulan data metrics diatas, mencari nilai minimum, maksimum, dan rata-rata.
- kedua task diatas akan dijalankan dengan cron, setiap semenit untuk catat metrics, dan setiap jam untuk mengolah metrics-metrics tersebut

**minute_log.sh**
1. create parent directory untuk menyimpan hasil
```shell
mkdir -p "$HOME/log/"
```
2. mengolah file menggunakan awk dengan meng-pipe ```free -m && du -sh```. lalu print masing-masing data dari command tersebut
   . lalu di outputkan ke log file
```shell
{ free -m | awk '{if(NR==2){printf $2","$3""$4","$5","$6","$7","}if(NR==3){printf $2","$3","$4","}}' ; du -sh /"$HOME"/ | awk '{printf $2","$1"\n"}' ; } >> "$log_file"
```
3. chmod file agar write only ke user masing masing
```shell
chmod 400 "$log_file"
```

**aggregate_minute_to_hourly.sh**
1. create parent directory untuk menyimpan hasil
```shell
mkdir -p "$HOME/log/"
```

2. dapatkan semua file dalam folder log pada jam terentu
```shell
{ readarray -t fs <<< "$(find /"$HOME"/log/*metrics_"$(date '+%Y%m%d%H')"*)";
```
3. loop setiap file, dan dapatkan data line kedua pakai sed
```shell
 for i in "${fs[@]}"; do sed -n "2p" "$i"; done; }
```
4. pipe hasilnya, lalu cut column 1 - 10 (column yang mau kita proses), sort dan dapatkan line paling atas (pakai head) untuk dapat nilai minumum lalu outputkan ke file aggregasi
```shell
| cut -d"," -f1-10 | sort -n | head -1 >> "$log_file"
```
5. lakukan hal yang sama seperti diatas untuk mencari nilai maksimum, tapi sekarang pakai tail untuk mencari nilai hasil sort yang paling bawah
```shell
| cut -d"," -f1-10 | sort -n | tail -1 >> "$log_file"
```
6. cari nilai rata-rata menggunakan awk dengan memasukan jumlah setiap column ke array, dan bagi dengan jumlah file per jam
```shell
| awk -F'[,]' '{ for (i=1;i<=NF;i++){if(i==9) {sum[i]=$i} else {sum[i]+=$i}} } END { printf "average,"; for (i=1;i<=NF;i++){if(i==9) {printf sum[i]} else {printf sum[i]/NR}; if (i!=NF) {printf ","} else {printf "\n"}}}' >> "$log_file"
```

7. chmod file agar write only ke user masing masing
```shell
chmod 400 "$log_file"
```
